import React, { Component } from "react";
import PropTypes from "prop-types";

// Styles
import "./style.scss";

class Avatar extends Component {
  static propTypes = {
    fileName: PropTypes.string,
    src: PropTypes.string,
    alt: PropTypes.string,
    sticker: PropTypes.object
  };

  static defaultProps = {
    fileName: "",
    src: "",
    alt: "",
    sticker: null
  };

  render() {
    const { fileName, src, alt, sticker } = this.props;

    if (!src && !fileName) {
      return null;
    }

    return (
      <div className="avatar-wrapper">
        <img
          className="avatar"
          src={src || `/assets/images/${fileName}`}
          alt={alt}
        />
        {sticker && (
          <img
            className="sticker"
            src={sticker.src || `/assets/images/${sticker.fileName}`}
            alt={sticker.stickerAlt}
          />
        )}
      </div>
    );
  }
}

export default Avatar;
