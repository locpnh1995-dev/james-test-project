import React, { Component } from "react";
import { Table } from "reactstrap";

// Styles
import "./style.scss";

class MyTable extends Component {
  render() {
    const { thead, tbody, className } = this.props;
    return (
      <Table className={`my-table${className ? ` ${className}` : ""}`}>
        <thead>{thead}</thead>
        <tbody>{tbody}</tbody>
      </Table>
    );
  }
}

export default MyTable;
