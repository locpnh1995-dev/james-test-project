import React, { Component } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

// Styles
import "./style.scss";

class MyIcon extends Component {
  render() {
    return <FontAwesomeIcon className="icon" {...this.props} />;
  }
}

export default MyIcon;
