import React, { Component } from "react";

// Components
import { Button } from "reactstrap";

// Styles
import "./style.scss";

class MyButton extends Component {
  // NOTE: We also modify the style to get correct UI. But requirements got "src/components" so I create this.
  render() {
    return <Button {...this.props}>{this.props.children}</Button>;
  }
}

export default MyButton;
