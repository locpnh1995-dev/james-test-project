import { library } from "@fortawesome/fontawesome-svg-core";
import {
  faCreditCard,
  faUser,
  faCalendar,
  faInfoCircle,
  faHandHoldingUsd,
  faFileSignature
} from "@fortawesome/free-solid-svg-icons";

library.add(
  faCreditCard,
  faUser,
  faCalendar,
  faInfoCircle,
  faHandHoldingUsd,
  faFileSignature
);
