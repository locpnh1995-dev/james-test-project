import React, { Component } from "react";
import classnames from "classnames";

// Components
import { TabContent, TabPane, Nav, NavItem, NavLink } from "reactstrap";

// Sub container
import Billing from "./Billing";

// Styles
import "./style.scss";

// Constants
import { tabs } from "./constants";

class FullInfo extends Component {
  constructor(props) {
    super(props);

    this.state = {
      activeTab: "3"
    };
  }

  toggle = tab => {
    if (this.state.activeTab !== tab) {
      this.setState({
        activeTab: tab
      });
    }
  };

  render() {
    return (
      <div className="full-info">
        <Nav tabs>
          {tabs.map(tab => (
            <NavItem
              key={tab.id}
              className={classnames({
                active: this.state.activeTab === tab.id
              })}
            >
              <NavLink
                onClick={() => {
                  this.toggle(tab.id);
                }}
              >
                {tab.name}
              </NavLink>
            </NavItem>
          ))}
        </Nav>
        <TabContent activeTab={this.state.activeTab}>
          {tabs.map(tab => {
            if (tab.id === "3") {
              return (
                <TabPane key={tab.id} tabId={tab.id}>
                  <Billing />
                </TabPane>
              );
            }

            return (
              <TabPane key={tab.id} tabId={tab.id}>
                <h4 className="title">{tab.name}</h4>
              </TabPane>
            );
          })}
        </TabContent>
      </div>
    );
  }
}

export default FullInfo;
