export const tabs = [
  {
    id: "1",
    name: "notes"
  },
  {
    id: "2",
    name: "details"
  },
  {
    id: "3",
    name: "billing"
  },
  {
    id: "4",
    name: "invoices"
  },
  {
    id: "5",
    name: "settings"
  }
];
