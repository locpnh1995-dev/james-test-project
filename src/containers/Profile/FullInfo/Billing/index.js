import React, { Component } from "react";

// Components
import { MyIcon, MyTable, MyButton } from "../../../../components";

// Sytles
import "./style.scss";

// Constants
import { statistics, extraInfos, serviceCharges } from "./constants";

const StatisticItem = props => {
  const { statistic } = props;

  if (statistic) {
    const { amount, label, description, icon } = statistic;
    return (
      <div className="statistic-item">
        <div className="statistic-value">
          <p className="amount">
            ${amount.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}
          </p>
          <p className="label">{label}</p>
          {description && <p className="description">{description}</p>}
        </div>
        <MyIcon icon={icon} />
      </div>
    );
  }
};

const ExtraInfo = props => {
  const { extraInfo } = props;

  if (extraInfo) {
    const { code, label, value, icon } = extraInfo;
    return (
      <div className={`extra-info-item ${code}`}>
        <MyIcon icon={icon} />
        <p className="label">
          {label}
          {code === "question-amend" ? "" : ":"}
        </p>
        {code === "question-amend" ? (
          <a className="value" href="/">
            {value}
          </a>
        ) : (
          <p className="value">{value}</p>
        )}
      </div>
    );
  }
};

class Billing extends Component {
  render() {
    return (
      <div className="summary-info-wrapper billing">
        <div className="statistic-info">
          <h4 className="title">CHARGES SUMMARY FOR JANUARY 2019</h4>
          <div className="statistic-list">
            {statistics.map((statistic, index) => (
              <StatisticItem key={index} statistic={statistic} />
            ))}
          </div>
          <div className="extra-info-list">
            {extraInfos.map(extraInfo => (
              <ExtraInfo key={extraInfo.code} extraInfo={extraInfo} />
            ))}
          </div>
          <div className="service-charges">
            <h4 className="title">SERVICE CHARGES</h4>
            <MyTable
              className="service-charge-table"
              thead={
                <React.Fragment>
                  <tr>
                    <th>Item</th>
                    <th>Details</th>
                    <th>Amount</th>
                    <th>Added on</th>
                    <th />
                  </tr>
                </React.Fragment>
              }
              tbody={
                <React.Fragment>
                  {serviceCharges.map((service, index) => (
                    <tr key={index}>
                      <td>{service.name}</td>
                      <td>
                        {service.details}
                        {service.creditApplied && (
                          <span className="credit-applied">Credit applied</span>
                        )}
                      </td>
                      <td>{`${service.amount < 0 ? "-" : ""}$${parseFloat(
                        Math.abs(service.amount)
                      ).toFixed(2)}`}</td>
                      <td>{service.addedOn}</td>
                      <td>
                        <MyButton className="btn-ghost" color="primary">
                          Edit
                        </MyButton>
                      </td>
                    </tr>
                  ))}
                </React.Fragment>
              }
            />
          </div>
        </div>
      </div>
    );
  }
}

export default Billing;
