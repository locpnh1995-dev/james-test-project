export const statistics = [
  {
    amount: 2350,
    icon: "user",
    label: "Membership Subtotal",
    description: ""
  },
  {
    amount: 503.98,
    icon: "credit-card",
    label: "Service Subtotal",
    description: ""
  },
  {
    amount: 2853.98,
    icon: "file-signature",
    label: "Next invoice Total",
    description: "Include the discount of $300"
  }
];

export const extraInfos = [
  {
    code: "billing-cycle",
    icon: "calendar",
    label: "Next Billing Cycle",
    value: "01/03/2019"
  },
  {
    code: "booking-credit",
    icon: "hand-holding-usd",
    label: "Booking Credit",
    value: "5 hours p/m"
  },
  {
    code: "question-amend",
    icon: "info-circle",
    label: "Questions or amends?",
    value: "Contact us"
  }
];

export const serviceCharges = [
  {
    name: "Mackenzies Room",
    details: "2hrs | 9:00am - 11:00am",
    creditApplied: true,
    amount: 0,
    addedOn: "07/01/2019"
  },
  {
    name: "Bondi Room",
    details: "2.5hrs | 10:00am - 12:30pm",
    creditApplied: true,
    amount: 0,
    addedOn: "09/01/2019"
  },
  {
    name: "Mackenzies Room",
    details: "1.5hrs | 2:00pm - 3:30pam",
    creditApplied: false,
    amount: 50,
    addedOn: "14/01/2019"
  },
  {
    name: "Mackenzies Room",
    details: "1.5hrs | 4:00pm - 5:30pm",
    creditApplied: false,
    amount: 75,
    addedOn: "16/01/2019"
  },
  {
    name: "Catering",
    details: "Catering for meeting on Weds 16 Jan",
    creditApplied: false,
    amount: 100,
    addedOn: "16/01/2019"
  },
  {
    name: "10 Day Pass",
    details: "Request from G.Brewing on Tues Jan 18",
    creditApplied: false,
    amount: 150,
    addedOn: "18/01/2019"
  },
  {
    name: "Bondi Room",
    details: "2.5hrs | 10:00am - 12:30pm",
    creditApplied: false,
    amount: 80,
    addedOn: "21/01/2019"
  },
  {
    name: "Catering",
    details: "Catering for meeting on Weds 21 Jan",
    creditApplied: false,
    amount: 93.98,
    addedOn: "21/01/2019"
  },
  {
    name: "Rewards",
    details: "Referral reward",
    creditApplied: false,
    amount: -45,
    addedOn: "01/01/2019"
  }
];
