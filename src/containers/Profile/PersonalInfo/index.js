import React, { Component } from "react";

// Components
import { Avatar, MyButton } from "../../../components";

// Sytles
import "./style.scss";

// Constants
const personalInfo = {
  fullName: "RON BURGANDY",
  title: "CEO & Anchor",
  email: "ron@channel4corp.com",
  mobile: "+61429654123",
  company: "CHANNEL 4 CORP",
  role: "Media agency",
  branchName: "OFFICE #2",
  branchLocation: "Bondi Junction"
};

class PersonalInfo extends Component {
  render() {
    const {
      fullName,
      title,
      email,
      mobile,
      company,
      role,
      branchName,
      branchLocation
    } = personalInfo;
    return (
      <div className="personal-info">
        <Avatar fileName="avatar.jpg" sticker={{ fileName: "sticker-4.png" }} />
        <div className="info-wrapper">
          <h6 className="full-name">{fullName}</h6>
          <p className="title">{title}</p>
          <p className="email">{email}</p>
          <p className="mobile">{mobile.replace(/([\S]{3})/g, "$1 ")}</p>
          <p className="company">{company}</p>
          <p className="role">{role}</p>
          <p className="branch-name">{branchName}</p>
          <p className="branch-location">{branchLocation}</p>
        </div>
        <MyButton className="message" color="primary">
          Message
        </MyButton>
      </div>
    );
  }
}

export default PersonalInfo;
