import React, { Component } from "react";
import { Helmet } from "react-helmet";

// Sub Containers
import PersonalInfo from "./PersonalInfo";
import FullInfo from "./FullInfo";

// Styles
import "./style.scss";

class ProfilePage extends Component {
  render() {
    return (
      <div className="profile-page">
        <Helmet>
          <title>Profile Page - James's Test Project By Loc Pham</title>
        </Helmet>
        <div className="left-content">
          <PersonalInfo />
        </div>
        <div className="main-content">
          <FullInfo />
        </div>
      </div>
    );
  }
}

export default ProfilePage;
