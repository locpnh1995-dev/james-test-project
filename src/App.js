import React, { Component } from "react";
import { Helmet } from "react-helmet";

// Containers
import { ProfilePage } from "./containers";

// Configs
import "./config/icons";

// Styles
import "bootstrap/dist/css/bootstrap.min.css";
import "./styles/theme.scss";

class App extends Component {
  render() {
    return (
      <div className="james-test-project">
        <Helmet>
          <meta charSet="utf-8" />
        </Helmet>
        <ProfilePage />
      </div>
    );
  }
}

export default App;
